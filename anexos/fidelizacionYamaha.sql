CREATE DATABASE IF NOT EXISTS `fidelizacionYamaha` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `fidelizacionyamaha`;

-- Tabla cliente
CREATE TABLE cliente (
    id INT AUTO_INCREMENT PRIMARY KEY,
    cedula VARCHAR(20) NOT NULL UNIQUE,
    nombres VARCHAR(100) NOT NULL,
    apellidos VARCHAR(100) NOT NULL,
    direccion VARCHAR(255) NOT NULL,
    fecha_nacimiento DATE NOT NULL,
    genero ENUM('m', 'f', 'otro') NOT NULL,
    celular VARCHAR(20) NOT NULL,
    email VARCHAR(100) NOT NULL,
    activo BOOLEAN NOT NULL DEFAULT TRUE,
    fecha_crea DATETIME NOT NULL
);

CREATE INDEX idx_cedula ON cliente (cedula);
CREATE INDEX idx_nombre ON cliente (nombres, apellidos);
CREATE INDEX idx_email ON cliente (email);


-- Tabla categoria
CREATE TABLE categoria (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    activo BOOLEAN NOT NULL DEFAULT TRUE,
    fecha_crea DATETIME NOT NULL
);

CREATE INDEX idx_nombre_categoria ON categoria (nombre);

-- Tabla vehiculo
CREATE TABLE vehiculo (
    id INT AUTO_INCREMENT PRIMARY KEY,
    numero_motor VARCHAR(50) NOT NULL UNIQUE,
    modelo VARCHAR(100) NOT NULL,
    cilindraje INT NOT NULL,
    color VARCHAR(50) NOT NULL,
    fecha_ensamble DATE NOT NULL,
    año_modelo YEAR NOT NULL,
    categoria_id INT,
    activo BOOLEAN NOT NULL DEFAULT TRUE,
    fecha_crea DATETIME NOT NULL,
    FOREIGN KEY (categoria_id) REFERENCES categoria(id)
);

CREATE INDEX idx_modelo ON vehiculo (modelo);
CREATE INDEX idx_fecha_ensamble ON vehiculo (fecha_ensamble);

-- Tabla vendedor
CREATE TABLE vendedor (
    id INT AUTO_INCREMENT PRIMARY KEY,
    cedula VARCHAR(20) NOT NULL UNIQUE,
    nombres VARCHAR(100) NOT NULL,
    apellidos VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL,
    direccion VARCHAR(255) NOT NULL,
    fecha_nacimiento DATE NOT NULL,
    genero ENUM('m', 'f', 'otro') NOT NULL,
    celular VARCHAR(20) NOT NULL,
    activo BOOLEAN NOT NULL DEFAULT TRUE,
    fecha_crea DATETIME NOT NULL
);

CREATE INDEX idx_cedula ON vendedor (cedula);
CREATE INDEX idx_nombre ON vendedor (nombres, apellidos);
CREATE INDEX idx_email ON vendedor (email);

-- Tabla ciudad
CREATE TABLE ciudad (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    codigo_dane VARCHAR(20) NOT NULL,
    activo BOOLEAN NOT NULL DEFAULT TRUE,
    fecha_crea DATETIME NOT NULL
);

CREATE INDEX idx_nombre_ciudad ON ciudad (nombre);
CREATE INDEX idx_codigo_dane ON ciudad (codigo_dane);

-- Tabla tienda
CREATE TABLE tienda (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    direccion VARCHAR(255) NOT NULL,
    ciudad_id INT,
    activo BOOLEAN NOT NULL DEFAULT TRUE,
    fecha_crea DATETIME NOT NULL,
    FOREIGN KEY (ciudad_id) REFERENCES ciudad(id)
);

CREATE INDEX idx_nombre_tienda ON tienda (nombre);

-- Tabla venta
CREATE TABLE venta (
    id INT AUTO_INCREMENT PRIMARY KEY,
    fecha DATE NOT NULL,
    vehiculo_id INT,
    vendedor_id INT,
    cliente_id INT,
    tienda_id INT,
    numero_factura VARCHAR(50) NOT NULL UNIQUE,
    precio DECIMAL(10, 2) NOT NULL,
    fecha_crea DATETIME NOT NULL,
    FOREIGN KEY (vehiculo_id) REFERENCES vehiculo(id),
    FOREIGN KEY (vendedor_id) REFERENCES vendedor(id),
    FOREIGN KEY (cliente_id) REFERENCES cliente(id),
    FOREIGN KEY (tienda_id) REFERENCES tienda(id)
);

CREATE INDEX idx_fecha ON venta (fecha);